#!/bin/bash
. bean_func.fnc

display=""
result=""
br="
"

# 引数が無ければ終了
if [ $# -gt 0 ] ; then
	for i in "$@" ; do
		filelist="${filelist}${br}${i}"
	done
	filelist=$(echo "${filelist}" | tail -n +2)
else
	filelist=$(yad --title="linuxBean アーカイブのリネーム" \
	--window-icon=application-x-archive --image=application-x-archive \
	--borders=10 --on-top --center --dnd \
	--text "このウインドウにアーカイブをドロップした後、OKを押してください。\n\nアーカイブのファイル名を、中身のフォルダと同じ名前に揃えます。")
	
	if [ $? -gt 0 ] ; then
		exit 1
	fi
	
	filelist=$(Bean_PathConv "${filelist}" | sort | uniq)
fi

if [ "${filelist}" == "" ] ; then
	exit 1
fi

# 区切り文字を改行に
_IFS="$IFS";IFS="${br}"

# リストを順番に処理
for i in $filelist ; do
	
	dir=$(Bean_FullDir "${i}")
	
	if [ $(basename "${i}" | grep -o  "\." | wc -l) -eq 0 ] || \
	   [ $(basename "${i}" | grep -o "^\." | wc -l) -gt 0 ]  ; then
		noext=$(basename "${i}")
		ext=""
	else
		noext=$(Bean_NoExt "${i}")
		ext=.$(Bean_Ext "${i}")
	fi
	
	if [ ! -f "${i}" ] ; then
		display="${display}FALSE|${dir}${noext}${ext}|（存在しません）|n|${br}"
		result="${result}FALSE${br}${dir}${noext}${ext}${br}（存在しません）${br}n${br}"
		continue
	fi
	
	if   [ $(echo "${i}" | grep ".tar." | wc -l ) -gt 0 ]  ; then
		if   [ $(echo "${i}" | grep ".tar.gz$"   | wc -l ) -gt 0 ]  ; then
			ext=".tar.gz"
			noext=$(echo "${noext}" | sed -e 's|.tar$||igm')
		elif [ $(echo "${i}" | grep ".tar.bz2$"  | wc -l ) -gt 0 ]  ; then
			ext=".tar.bz2"
			noext=$(echo "${noext}" | sed -e 's|.tar$||igm')
		elif [ $(echo "${i}" | grep ".tar.xz$"   | wc -l ) -gt 0 ]  ; then
			ext=".tar.xz"
			noext=$(echo "${noext}" | sed -e 's|.tar$||igm')
		elif [ $(echo "${i}" | grep ".tar.bz$"  | wc -l ) -gt 0 ]  ; then
			ext=".tar.bz"
			noext=$(echo "${noext}" | sed -e 's|.tar$||igm')
		elif [ $(echo "${i}" | grep ".tar.7z$"   | wc -l ) -gt 0 ]  ; then
			ext=".tar.7z"
			noext=$(echo "${noext}" | sed -e 's|.tar$||igm')
		elif [ $(echo "${i}" | grep ".tar.lzma$" | wc -l ) -gt 0 ]  ; then
			ext=".tar.lzma"
			noext=$(echo "${noext}" | sed -e 's|.tar$||igm')
		fi
	elif [ $(echo "${i}" | grep ".TAR." | wc -l ) -gt 0 ]  ; then
		if   [ $(echo "${i}" | grep ".TAR.GZ$"   | wc -l ) -gt 0 ]  ; then
			ext=".TAR.GZ"
			noext=$(echo "${noext}" | sed -e 's|.TAR$||igm')
		elif [ $(echo "${i}" | grep ".TAR.BZ2$"  | wc -l ) -gt 0 ]  ; then
			ext=".TAR.BZ2"
			noext=$(echo "${noext}" | sed -e 's|.TAR$||igm')
		elif [ $(echo "${i}" | grep ".TAR.XZ$"   | wc -l ) -gt 0 ]  ; then
			ext=".TAR.XZ"
			noext=$(echo "${noext}" | sed -e 's|.TAR$||igm')
		elif [ $(echo "${i}" | grep ".TAR.BZ$"  | wc -l ) -gt 0 ]  ; then
			ext=".TAR.BZ"
			noext=$(echo "${noext}" | sed -e 's|.tar$||igm')
		elif [ $(echo "${i}" | grep ".TAR.7Z$"   | wc -l ) -gt 0 ]  ; then
			ext=".TAR.7Z"
			noext=$(echo "${noext}" | sed -e 's|.TAR$||igm')
		elif [ $(echo "${i}" | grep ".TAR.LZMA$" | wc -l ) -gt 0 ]  ; then
			ext=".TAR.LZMA"
			noext=$(echo "${noext}" | sed -e 's|.TAR$||igm')
		fi
	fi
		
	cd "${dir}"
	
	if   [ "${ext}" == ".zip" ] || [ "${ext}" == ".ZIP" ] ; then
		if [ $(Bean_CmdExist unzip) -gt 0 ] ; then
			display="${display}FALSE|${dir}${noext}${ext}|（unzipがありません）|n|${br}"
			result="${result}FALSE${br}${dir}${noext}${ext}${br}（unzipがありません）${br}n${br}"
			continue
		fi
		line=$(unzip -l "${i}" | \
			grep -v "[0-9][0-9]\s*\._.*$" | \
			grep -v "\s*Thumbs\.db$"   | \
			grep -v "\s*Desktop\.ini$" | \
			grep -v "\s*desktop\.ini$" | \
			grep -v "\s*\.DS_Store$"   | \
			grep -v "\s*\.svn$"        | \
			grep -v "\s*_notes$"       | \
			grep -v "\s*\.BridgeSort$" | \
			grep -v "\s*\.BridgeLabelsAndRatings$" | \
			head -n 4 | tail -n 1)
		
		
		if [ $(echo "${line}" | grep -o "/" | wc -l) -gt 0 ] ; then
			size=0
			name=$(echo "${line}" | sed \
				-e 's:^\s*\S*\s*\S*\s*\S*\s*::igm' | \
				awk -F"/" '{print $1}')
		else
			size=$(echo "${line}" | awk '{print $1}')
			name=$(echo "${line}" | \
				sed -e 's|^\s*\S*\s*\S*\s*\S*\s*||igm' -e 's|/$||igm')
		fi
			
	elif [ "${ext}" == ".rar"  ] || [ "${ext}" == ".RAR" ]  ; then
		if [ $(Bean_CmdExist unrar) -gt 0 ] ; then
			display="${display}FALSE|${dir}${noext}${ext}|（unrarがありません）|n|${br}"
			result="${result}FALSE${br}${dir}${noext}${ext}${br}（unrarがありません）${br}n${br}"
			continue
		fi
		
		line=$(unrar l "${i}" | \
			grep -v "^\s*\._" | \
			grep -v "^\s*Thumbs\.db"   | \
			grep -v "^\s*Desktop\.ini" | \
			grep -v "^\s*desktop\.ini" | \
			grep -v "^\s*\.DS_Store"   | \
			grep -v "^\s*\.svn"        | \
			grep -v "^\s*_notes"       | \
			grep -v "^\s*\.BridgeSort" | \
			grep -v "^\s*\.BridgeLabelsAndRatings" | \
			tail -n 4 | head -n 1)
		
		size=$(echo "${line}" | sed \
			-e 's|\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S\s*\S*$||igm' \
			-e 's|^\s*\S*\s*||igm')
			
		name=$(echo "${line}" | sed \
			-e 's|\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S\s*\S*$||igm' \
			-e 's|^\s||igm')
		
	elif [ "${ext}" == ".lzh" ] || [ "${ext}" == ".LZH" ] || \
	     [ "${ext}" == ".lha" ] || [ "${ext}" == ".LHA" ] ; then
		if [ $(Bean_CmdExist lha) -gt 0 ] ; then
			display="${display}FALSE|${dir}${noext}${ext}|（lhaがありません）|n|${br}"
			result="${result}FALSE${br}${dir}${noext}${ext}${br}（lhaがありません）${br}n${br}"
			continue
		fi
		
		line=$(lha l "${i}" | \
			grep -v "[0-9][0-9]\s*\._.*$" | \
			grep -v "\s*Thumbs\.db$"   | \
			grep -v "\s*Desktop\.ini$" | \
			grep -v "\s*desktop\.ini$" | \
			grep -v "\s*\.DS_Store$"   | \
			grep -v "\s*\.svn$"        | \
			grep -v "\s*_notes$"       | \
			grep -v "\s*\.BridgeSort$" | \
			grep -v "\s*\.BridgeLabelsAndRatings$" | \
			head -n 3 | tail -n 1)
		
		if [ $(echo "${line}" | grep "^\[" | wc -l) -gt 0 ] ; then
			size=$(echo "${line}" | awk '{print $2}')
			name=$(echo "${line}" | sed \
				-e 's|^\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*||igm' -e 's|/$||igm')
		else
			size=$(echo "${line}" | awk '{print $3}')
			name=$(echo "${line}" | sed \
				-e 's|^\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*||igm' -e 's|/$||igm')
		fi
		
	elif [ "${ext}" == ".7z"  ] || [ "${ext}" == ".7Z" ] ; then
		if [ $(Bean_CmdExist 7z) -gt 0 ] ; then
			display="${display}FALSE|${dir}${noext}${ext}|（7zがありません）|n|${br}"
			result="${result}FALSE${br}${dir}${noext}${ext}${br}（7zがありません）${br}n${br}"
			continue
		fi
		
		line=$(7z l "${i}" | tail -n +18 | head -n -2 | grep -v / | \
			grep -v "[0-9]\s*\._.*$"   | \
			grep -v "\s*Thumbs\.db$"   | \
			grep -v "\s*Desktop\.ini$" | \
			grep -v "\s*desktop\.ini$" | \
			grep -v "\s*\.DS_Store$"   | \
			grep -v "\s*\.svn$"        | \
			grep -v "\s*_notes$"       | \
			grep -v "\s*\.BridgeSort$" | \
			grep -v "\s*\.BridgeLabelsAndRatings$" | \
			grep "[0-9]\sD\.\.\.\.")
			
		size=$(echo "${line}" | awk '{print $4}')
		name=$(echo "${line}" | sed \
			's|^\S*\s\S*\s\S*\s*\S*\s*\S*\s*||igm')
		
	elif [ "${ext}" == ".cab"  ] || [ "${ext}" == ".CAB" ]  ; then
		if [ $(Bean_CmdExist cabextract) -gt 0 ] ; then
			display="${display}FALSE|${dir}${noext}${ext}|（cabextractがありません）|n|${br}"
			result="${result}FALSE${br}${dir}${noext}${ext}${br}（cabextractがありません）${br}n${br}"
			continue
		fi
		line=$(cabextract -l "${i}" | \
			grep -v "|\s*\._.*$" | \
			grep -v "\s*Thumbs\.db$"   | \
			grep -v "\s*Desktop\.ini$" | \
			grep -v "\s*desktop\.ini$" | \
			grep -v "\s*\.DS_Store$"   | \
			grep -v "\s*\.svn$"        | \
			grep -v "\s*_notes$"       | \
			grep -v "\s*\.BridgeSort$" | \
			grep -v "\s*\.BridgeLabelsAndRatings$" | \
			head -n 4 | tail -n 1)
		size=0
		#size=$(echo "${line}" | awk '{print $1}')
		name=$(echo "${line}" | sed \
			-e 's:^\s*\S*\s*|\s*\S*\s*\S*\s*|\s*::igm' | \
			awk -F"/" '{print $1}')

	elif [ "${ext}" == ".tar"  ] || [ "${ext}" == ".TAR"      ] ||
	     [ "${ext}" == ".tgz"  ] || [ "${ext}" == ".tar.gz"   ] ||
	     [ "${ext}" == ".tbz2" ] || [ "${ext}" == ".tar.bz2"  ] ||
	     [ "${ext}" == ".txz"  ] || [ "${ext}" == ".tar.xz"   ] ||
	     [ "${ext}" == ".tbz"  ] || [ "${ext}" == ".tar.bz"   ] ||
	     [ "${ext}" == ".tzma" ] || [ "${ext}" == ".tar.lzma" ] ||
	     [ "${ext}" == ".TGZ"  ] || [ "${ext}" == ".TAR.GZ"   ] ||
	     [ "${ext}" == ".TBZ2" ] || [ "${ext}" == ".TAR.BZ2"  ] ||
	     [ "${ext}" == ".TXZ"  ] || [ "${ext}" == ".TAR.XZ"   ] ||
	     [ "${ext}" == ".TBZ"  ] || [ "${ext}" == ".TAR.BZ"   ] ||
	     [ "${ext}" == ".TZMA" ] || [ "${ext}" == ".TAR.TZMA" ] ; then
	     
		line=$(tar tvf "${i}" | \
			grep -v "[0-9][0-9]\s*\._.*$" | \
			grep -v "\s*Thumbs\.db$"   | \
			grep -v "\s*Desktop\.ini$" | \
			grep -v "\s*desktop\.ini$" | \
			grep -v "\s*\.DS_Store$"   | \
			grep -v "\s*\.svn$"        | \
			grep -v "\s*_notes$"       | \
			grep -v "\s*\.BridgeSort$" | \
			grep -v "\s*\.BridgeLabelsAndRatings$" | \
			tail -n 1)
		size=0
		#size=$(echo "${line}" | awk '{print $3}')
		name=$(echo "${line}" | sed \
			-e 's|^\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*||igm' | \
			awk -F"/" '{print $1}')
			
	else
		display="${display}FALSE|${dir}${noext}${ext}|（変更なし）|n|${br}"
		result="${result}FALSE${br}${dir}${noext}${ext}${br}（変更なし）${br}n${br}"
		continue
	fi
	
	if [ ${size} -ne 0 ] ; then
		display="${display}FALSE|${dir}${noext}${ext}|（変更なし）|n|${br}"
		result="${result}FALSE${br}${dir}${noext}${ext}${br}（変更なし）${br}n${br}"
	else
		display="${display}FALSE|${dir}${noext}${ext}|${dir}${name}${ext}|y|${br}"
		result="${result}FALSE${br}${dir}${noext}${ext}${br}${dir}${name}${ext}${br}y${br}"
	fi
done

# 区切り文字を戻す
IFS="$_IFS"

display=$(echo "${display}" | head -n -1)
result=$(echo "${result}" | head -n -1)

result=$(echo "${result}" | yad --width="640" --height="480" --on-top --center \
--title="linuxBean アーカイブのリネーム" --window-icon=application-x-archive \
--text "リネームするファイルにチェックを入れ、OKを押してください。\n（※リネーム後に名前が重複する場合は処理がスキップされます。）" \
--list --checklist --hide-column="4" \
--column="実行" --column="変更前" --column="変更後" --column="種別" | \
grep "|y|$" )

if [ $(echo "${result}" | grep -v "^$" | wc -l ) -gt 0 ] ; then
	
	# 区切り文字を改行に設定
	_IFS="${IFS}";IFS="${br}"
	
	# ループ
	for line in $(echo "${result}") ; do
		before=$(echo "${line}" | awk -F"|" '{print $2}')
		after=$(echo "${line}" | awk  -F"|" '{print $3}')
		mv -n "${before}" "${after}"
	done
	
	# 区切り文字を戻す
	IFS="${_IFS}"

	yad --title="linuxBean アーカイブのリネーム" --on-top --center \
	--window-icon=application-x-archive --image=application-x-archive \
	--text "リネームが完了しました。"
else
	exit 1
fi
