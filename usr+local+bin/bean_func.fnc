
Bean_FullPath()
{
	echo $(readlink -f "$1")
}

Bean_WinPath()
{
	if [ $# -gt 0 ] ; then
		echo $(winepath -w "$1")
	else
		echo
	fi
}

Bean_FullDir()
{
	local fullpath=$(readlink -f "$1")
	local filename="${fullpath##*/}"
	local dir="${fullpath:0:${#fullpath} - ${#filename}}"
	echo "${dir}"
}

Bean_NoExt()
{
	local p=$(basename "$1")
	local n="${p%.*}"
	echo "${n}"
}

Bean_Ext()
{
	local p=$(basename "$1")
	local n="${p##*.}"
	echo "${n}"
}

Bean_PathConv()
{
	echo "$1" | sed -e 's|^\s*file://*|/|igm' | nkf --url-input --utf8
}

Bean_UserDirs()
{
	local adir
	adir=""
	adir=$(cat ${HOME}/.config/user-dirs.dirs | grep "$1" | grep -v ^\# | head -n 1 | \
		sed -e 's/^.*=//igm' -e 's/^\"//igm' -e 's/\"$//igm' -e "s/^\'//" -e "s/\'$//")
	if [ ! "${adir}" == "" ] ; then
		adir="echo \"${adir}\""
		adir=$(eval ${adir})
		echo "$adir"
	else
		if [ -d "$HOME/デスクトップ" ] ; then
			echo "$HOME/デスクトップ"
			
		elif [ -d "$HOME/Desktop" ] ; then
			echo "$HOME/Desktop"
			
		elif [ -d "$HOME/ドキュメント" ] ; then
			echo "$HOME/ドキュメント"
			
		elif [ -d "$HOME/Documents" ] ; then
			echo "$HOME/Documents"
	
		else
			echo "$HOME"
		fi
	fi
}

Bean_ProcExist()
{
	ps alxww | grep "$1" | grep -v grep | wc -l
}

Bean_CmdExist()
{
	which $1 > /dev/null
	echo $?
}

Bean_OpenWithFM()
{
	if   [ $(Bean_ProcExist pcmanfm) -gt 0 ] ; then
		pcmanfm "$1"
	elif [ $(Bean_ProcExist thunar) -gt 0 ] ; then
		thunar "$1"
	elif [ $(Bean_ProcExist Thunar) -gt 0 ] ; then
		Thunar "$1"
	elif [ $(Bean_ProcExist nautilus) -gt 0] ; then
		nautilus "$1"
	elif [ $(Bean_ProcExist Nautilus) -gt 0 ] ; then
		Nautilus "$1"
	elif [ -e "/usr/bin/pcmanfm" ] ; then
		pcmanfm "$1"
	elif [ -e "/usr/bin/thunar" ] ; then
		thunar "$1"
	elif [ -e "/usr/bin/Thunar" ] ; then
		Thunar "$1"
	elif [ -e "/usr/bin/nautilus" ] ; then
		nautilus "$1"
	elif [ -e "/usr/bin/Nautilus" ] ; then
		Nautilus "$1"
	fi
}

Bean_CopyToClip()
{
	echo "$1" | xsel -i -b
}

Bean_GetActivePID()
{
	local wid
	local pid
	wid=`xprop -root | awk '/_NET_ACTIVE_WINDOW\(WINDOW\)/{print $5}'`
	pid=`xprop -id "$wid" | awk '/_NET_WM_PID\(CARDINAL\)/{print $NF}'`
	echo "$pid"
}

Bean_GetActiveWName()
{
	local wid
	local wname
	wid=`xprop -root | awk '/_NET_ACTIVE_WINDOW\(WINDOW\)/{print $5}' | sed -e 's/,//' -e 's/^0x/0x0/'`
	wname=`wmctrl -l | grep "$wid" | sed -e 's/^0x[^ ]\+ \+[^ ]\+ \+[^ ]\+ \+//'`
	echo "$wname"
}

Bean_GetPID()
{
	local wid
	local pid
	wid=`wmctrl -l | grep $1 | awk '{print $1}' | sed -e 's/^0x0/0x/'`
	pid=`xprop -id "$wid" | awk '/_NET_WM_PID\(CARDINAL\)/{print $NF}'`
	echo "$pid"
}

Bean_GetWName()
{
	local wname
	wname=`wmctrl -l | grep $1 | sed -e 's/^0x[^ ]\+ \+[^ ]\+ \+[^ ]\+ \+//'`
	echo "$wname"
}


_echo_red()
{
	echo -e "\e[31m${@}\e[m"
}
_echo_blue()
{
	echo -e "\e[34m${@}\e[m"
}
_echo_green()
{
	echo -e "\e[32m${@}\e[m"
}
_echo_purple()
{
	echo -e "\e[35m${@}\e[m"
}