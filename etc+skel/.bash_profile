# ~/.bash_profile

_LANG=$LANG

case $TERM in
  linux) LANG=C ;;
  *) LANG=$_LANG ;;
esac
